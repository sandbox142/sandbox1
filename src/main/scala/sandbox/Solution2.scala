package sandbox

object Solution2 {
  /**
   * Finds all PromotionCombos with maximum number of combinable
   * promotions in each.
   *
   * @param allPromotions All available promotions
   * @return all PromotionCombos with maximum number of combinable promotions in each.
   */
  def allCombinablePromotions(allPromotions: Seq[Promotion]): Seq[PromotionCombo] = {
    val availablePromoCodes = allPromotions.map(_.code).toList
    val notCombinableMap = allPromotions.groupBy(_.code).view.mapValues(_.head.notCombinableWith.toSet)

    def buildSets(promoAcc: Vector[String], availablePromos: List[String]): LazyList[Seq[String]] =
      availablePromos match {
        case Nil => LazyList(promoAcc)
        case promoCode :: nextAvailablePromos =>
          val notCombinablePromos = notCombinableMap.get(promoCode).iterator.toSet.flatten
          val nextAvailablePromosFiltered = nextAvailablePromos.filterNot(notCombinablePromos.contains)
          lazy val availableCodesReduced = nextAvailablePromosFiltered.size != nextAvailablePromos.size
          buildSets(promoAcc :+ promoCode, nextAvailablePromosFiltered) #::: (
            // if available codes reduced skip current promo code to explore all combinations
            if (availableCodesReduced) buildSets(promoAcc, nextAvailablePromos) else LazyList.empty
          )
      }

    buildSets(Vector.empty, availablePromoCodes).map(PromotionCombo.apply)
  }

  /**
   * Same as [[allCombinablePromotions(Seq[Promotion])]] but for a specific promotion code
   * @param promotionCode Promotion code
   * @param allPromotions All available promotions
   */
  def combinablePromotions(promotionCode: String, allPromotions: Seq[Promotion]): Seq[PromotionCombo] =
    allCombinablePromotions(allPromotions)
      .filter(combo => combo.promotionCodes.contains(promotionCode))
      .map { combo =>
        combo.copy(promotionCodes = promotionCode +: combo.promotionCodes.filterNot(_ == promotionCode))
      }
}
