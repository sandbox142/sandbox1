package sandbox

object Solution1 {
  /**
   * For given rates and prices calculates best available cabin prices for each rate group
   *
   * @param rates Rate to rate group mappings
   * @param prices Available cabin prices
   * @return best available cabin prices for each rate group
   */
  def getBestGroupPrices(rates: Seq[Rate], prices: Seq[CabinPrice]): Seq[BestGroupPrice] = {
    val rateGroups = rates.groupBy(_.rateCode).flatMap { case (rateCode, rates) =>
      rates.headOption.map(rate => rateCode -> rate.rateGroup)
    }
    val ungroupedAndUnfiltered: Seq[BestGroupPrice] = prices
      .flatMap { case CabinPrice(cabinCode, rateCode, price) =>
        rateGroups.get(rateCode).map(BestGroupPrice(cabinCode, rateCode, price, _))
      }
    ungroupedAndUnfiltered
      .groupBy(bPrice => (bPrice.cabinCode, bPrice.rateGroup)).values
      .map { groupPrices =>
        groupPrices.minBy(_.price)
      }.toSeq
  }
}
