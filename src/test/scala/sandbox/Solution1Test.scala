package sandbox

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should

class Solution1Test extends AnyFlatSpec with should.Matchers {

  behavior of "Solution1"

  it should "calculate best prices for given example" in {
    val rates = Seq(
      Rate("M1", "Military"), Rate("M2", "Military"), Rate("S1", "Senior"), Rate("S2", "Senior"),
    )
    val cabinPrices = Seq(
      CabinPrice("CA", "M1", 200.00), CabinPrice("CA", "M2", 250.00),
      CabinPrice("CA", "S1", 225.00), CabinPrice("CA", "S2", 260.00),
      CabinPrice("CB", "M1", 230.00), CabinPrice("CB", "M2", 260.00),
      CabinPrice("CB", "S1", 245.00), CabinPrice("CB", "S2", 270.00),
    )
    Solution1.getBestGroupPrices(rates, cabinPrices) should contain theSameElementsAs Seq(
      BestGroupPrice("CA", "M1", 200.00, "Military"),
      BestGroupPrice("CA", "S1", 225.00, "Senior"),
      BestGroupPrice("CB", "M1", 230.00, "Military"),
      BestGroupPrice("CB", "S1", 245.00, "Senior"),
    )
  }
}
