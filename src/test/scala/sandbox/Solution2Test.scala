package sandbox

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should

class Solution2Test extends AnyFlatSpec with should.Matchers {

  val allPromos = Seq(
    Promotion("P1", Seq("P3")), // P1 is not combinable with P3
    Promotion("P2", Seq("P4", "P5")), // P2 is not combinable with P4 and P5
    Promotion("P3", Seq("P1")), // P3 is not combinable with P1
    Promotion("P4", Seq("P2")), // P4 is not combinable with P2
    Promotion("P5", Seq("P2")), // P5 is not combinable with P2
  )

  behavior of "Solution2Test"

  it should "allCombinablePromotions" in {
    Solution2.allCombinablePromotions(allPromos) should contain theSameElementsAs Seq(
      PromotionCombo(Seq("P1", "P2")),
      PromotionCombo(Seq("P1", "P4", "P5")),
      PromotionCombo(Seq("P2", "P3")),
      PromotionCombo(Seq("P3", "P4", "P5")),
    )
  }

  it should "combinablePromotions for P1" in {
    Solution2.combinablePromotions("P1", allPromos) should contain theSameElementsAs Seq(
      PromotionCombo(Seq("P1", "P2")),
      PromotionCombo(Seq("P1", "P4", "P5")),
    )
  }

  it should "combinablePromotions for P3" in {
    Solution2.combinablePromotions("P3", allPromos) should contain theSameElementsAs Seq(
      PromotionCombo(Seq("P3", "P2")),
      PromotionCombo(Seq("P3", "P4", "P5")),
    )
  }
}
